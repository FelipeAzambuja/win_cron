program Project1;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Classes,
  SysUtils,
  strutils { you can add units after this };

var
  i: integer;
  cron_list: TStringList;

  split: TStringList;
  cron_line: string;
  command: string;
  //dates
  minute: string;
  hour: string;
  day: string;
  month: string;
  weekday: string;
  decode_ms: word;
  decode_second: word;
  decode_minute: word;
  decode_hour: word;
  decode_day: word;
  decode_month: word;
  decode_year: word;
  decode_weekday: integer;

begin
  split := TStringList.Create;
  cron_list := TStringList.Create;
  while True do
  begin
    cron_list.LoadFromFile('cron.txt');
    for i := 0 to cron_list.Count - 1 do
    begin
      cron_line := cron_list[i];
      if Copy(cron_line, 0, 1) = '#' then
      begin
        // WriteLn(copy(cron_line, 2));
        Continue;
      end;
      split.Delimiter := #9; //tab
      split.DelimitedText := cron_line;
      minute := split[0];
      hour := split[1];
      day := split[2];
      month := split[3];
      weekday := split[4];
      DecodeDate(now(), decode_year, decode_month, decode_day);
      decode_weekday := DayOfWeek(now());
      DecodeTime(now(), decode_hour, decode_minute, decode_second, decode_ms);

      if (minute <> '*') and (IntToStr(decode_minute) <> minute) then
      begin
        continue;
      end;

      if (hour <> '*') and (IntToStr(decode_hour) <> hour) then
      begin
        continue;
      end;

      if (day <> '*') and (IntToStr(decode_day) <> day) then
      begin
        continue;
      end;

      if (month <> '*') and (IntToStr(decode_month) <> month) then
      begin
        continue;
      end;

      if (weekday <> '*') and (IntToStr(decode_weekday) <> weekday) then
      begin
        continue;
      end;

      //executa o comando
      writeln('executando o comando');
      readln;
      Sleep(500);
    end;
  end;
  cron_list.Free;
end.


